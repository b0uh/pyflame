# Copyright 2020-2022 Daniel Harding
# Distributed as part of the pyflame project under the terms of the MIT license


class MissingFlamegraphScript(Exception):
    pass
